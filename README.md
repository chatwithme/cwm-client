# ChatWithMe Front Library
## Installation
Firstly install the package with the command below in a nodejs project.
```shell
npm install @chatwithme/client
```
and after that you'll need to use another of our package in your back end [@chatwithme/server](https://www.npmjs.com/package/@chatwithme/server).
## Subscribe to message
```typescript
import { CWMChat } from '@chatwithme/client'
CWMChat.subscribeMessage((message)=> {
    // Handle message display and storage here
})
```

> A message that is send from the client to the back will be emitted in the subscribe.

## Once your user is logged

You can now start the chat by using the method ```start``` and passing the user token you got by using [@chatwithme/server](https://www.npmjs.com/package/@chatwithme/server)

```typescript
import {CWMChat} from "@chatwithme/client";

CWMChat.start('the token you got for your user with chatwithme/server')
```
## @chatwithme/server

Now you have to install the [@chatwithme/server](https://www.npmjs.com/package/@chatwithme/server) module in your back-end if it's not done yet !
