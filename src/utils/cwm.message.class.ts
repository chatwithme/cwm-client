import { CWMChat } from '../cwm.chat.class'

/**
 * Class that represent a message
 */
export class CwmMessage {
    private readonly _content: string
    private readonly _recipient: string | number
    private readonly _sender: string

    /**
     * Constructor of CwmMessageClass
     *
     * @param content Content of the message
     * @param recipient Target id of the message
     * @param sender Sender id of the message. Optional default user local id
     */
    constructor(
        content: string,
        recipient: string | number,
        sender?: string | undefined
    ) {
        this._content = content
        this._recipient = recipient
        this._sender = sender == undefined ? CWMChat.userId : sender
    }

    /**
     * Get the target id
     *
     * @return string Target id
     */
    public getRecipient(): string | number {
        return this._recipient
    }

    /**
     * Get the sender id
     *
     * @return string Sender id
     */
    public getSender(): string {
        return this._sender
    }

    /**
     * Get the content of the message
     *
     * @return string Message content
     */
    public getContent(): string {
        return this._content
    }

    /**
     * Check if the message is sent by the local user or not
     *
     * @return boolean true = sent by the local user
     */
    public isMe(): boolean {
        return this._sender == CWMChat.userId
    }
}
