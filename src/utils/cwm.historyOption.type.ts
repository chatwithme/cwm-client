export type CwmHistoryOptionType = {
    startDate?: Date
    endDate?: Date
    recipientId?: string
    skip?: number
    limit?: number
}
