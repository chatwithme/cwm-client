import { CWMChat } from './cwm.chat.class'
import * as dotenv from 'dotenv'
import { CwmMessage } from "./utils/cwm.message.class";

dotenv.config()

console.log(process.env.CWM_HTTP_LINK)
CWMChat.start(process.env.USER_TOKEN)
    .then(() => {
      /*CWMChat.getMessageHistoryWith({
            recipientId: '2',
        }).then(console.log)*/
      CWMChat.sendMessage(new CwmMessage(
        'Hello',
        5
      )).catch(e => {
        console.log(e)
      })
    })
    .catch((error) => {
        console.log('error ' + error)
        console.log(CWMChat.isStarted)
    })
