import { io, Socket } from 'socket.io-client'
import { Subject } from 'rxjs'
import { CwmMessage } from './utils/cwm.message.class'
import axios, { AxiosResponse } from 'axios'
import { CwmHistoryOptionType } from './utils/cwm.historyOption.type'

/**
 * Singleton for your chat powered by ChatWithMe
 */
export class CWMChat {
    private static INSTANCE: CWMChat
    protected static apiLink: string = ''
    protected static socketLink: string = ''

    private _subject: Subject<CwmMessage> = new Subject<CwmMessage>()

    private _socket: Socket
    private _userToken: string
    private _userId: string = ''
    private _isStarted: boolean = false

    protected set socket(socket: Socket) {
        this._socket = socket
    }

    protected get socket(): Socket {
        return this._socket
    }

    public static disconnect(): void {
        CWMChat.instance.isStarted = false
        CWMChat.instance.socket.close()
        CWMChat.instance.userId = ''
        CWMChat.instance._userToken = ''
    }

    protected set userToken(userToken: string) {
        this._userToken = userToken
        const rawUserData = this.userToken.split('.')[1]
        let deBufferedUserData = ''

        if (typeof atob != "undefined") {
            deBufferedUserData = atob(rawUserData)
        } else if (Buffer != undefined) {
            const buffer = Buffer.from(rawUserData, 'base64')
            deBufferedUserData = buffer.toString('utf8')
        } else {
            throw Error("No way to de buffer the user data")
        }

        this.userId = JSON.parse(deBufferedUserData)['externalUserId']
        return
    }

    protected get userToken(): string {
        return this._userToken
    }

    /**
     * Check if the chat is started
     *
     * @return boolean State of the CWMChat
     */
    protected set isStarted(isStarted: boolean) {
        this._isStarted = isStarted
    }

    public get isStarted(): boolean {
        return this._isStarted
    }

    public static get isStarted(): boolean {
        return this.instance.isStarted
    }

    protected set userId(userId: string) {
        this._userId = userId
    }

    /**
     * Get the user local chat id
     *
     * @return string CWMChat id of the local user
     */
    public static get userId(): string {
        return this.instance._userId
    }

    /**
     * Protected constructor to instantiate the singleton
     * @protected
     */
    protected constructor() {
        console.log(process.env.CWM_HTTP_LINK)
        CWMChat.apiLink = process.env.CWM_HTTP_LINK == undefined ? 'https://api.chatwithme.fr' : process.env.CWM_HTTP_LINK
        CWMChat.socketLink = process.env.CWM_WS_LINK == undefined ? 'wss://api.chatwithme.fr' : process.env.CWM_WS_LINK
    }

    /**
     * Static function to get the chat INSTANCE
     *
     * @return CWMChat Instance of CWMChat
     */
    private static get instance(): CWMChat {
        if (CWMChat.INSTANCE == undefined) {
            CWMChat.INSTANCE = new CWMChat()
        }
        return CWMChat.INSTANCE
    }

    /**
     * Initiate the chat instance with the endpoint given to get
     * the user token and start the socket client connection.
     *
     * @param endpoint { string } Endpoint that point the back end to get the token chat instance
     * @param cwmCredentials { CwmCredentials } Set the method of authentication to the endpoint
     */
    public static start(userToken: string): Promise<void> {
        const chatInstance: CWMChat = CWMChat.instance
        return new Promise<void>((resolve, reject) => {
            chatInstance.userToken = userToken
            chatInstance
                .initChannel()
                .then(() => {
                    resolve()
                })
                .catch(reject)
        })
    }

    /**
     * Used to init the socket connection with ChatWithMe Api
     *
     * @private
     */
    private initChannel(): Promise<void> {
        return new Promise<void>((resolve, reject) => {
            this.socket = io(`${CWMChat.socketLink}`, {
                extraHeaders: {
                    Authorization: this.userToken,
                },
            })

            this.socket.on('connect', () => {
                this.isStarted = true
                this.subscribeChannel()

                resolve()
            })

            this.socket.on('error', () => {
                reject('unable to connect server')
            })
        })
    }

    /**
     * Used to link message channel of the socket client and use rxjs Subject to emit event
     *
     * @private
     */
    private subscribeChannel(): void {
        this.socket.on('message', (messageRaw: string) => {
            let messageData = JSON.parse(messageRaw)
            this._subject.next(
                new CwmMessage(
                    messageData.content,
                    messageData.recipientId,
                    messageData.sender
                )
            )
        })
    }

    /**
     * Send a message to the ChatWithMe API to be sent to another user
     * The sent message will be emitted from the rxjs subject if no problem happen
     *
     * @param message Object CwmMessageClass to send
     *
     * @return Promise<void> Promise to know if the message has been sent successfully
     */
    public static sendMessage(message: CwmMessage): Promise<void> {
        return new Promise<void>((resolve, reject) => {
            axios
                .post(
                    [CWMChat.apiLink, 'webhooks', 'messages'].join('/'),
                    {
                        content: message.getContent(),
                        recipientId: message.getRecipient(),
                    },
                    {
                        headers: {
                            Authorization: `Bearer ${this.instance.userToken}`,
                            'Content-Type': 'application/json',
                        },
                    }
                )
                .then((response: AxiosResponse) => {
                    this.instance._subject.next(message)
                    resolve()
                })
                .catch(reject)
        })
    }

    /**
     * Fetch messages with a user or room id represented by the recipient id
     * and managed with some option
     *
     * @param opt Option to "filter". Optional
     *
     * @return Promise<CwmMessageClass[]> Promise of all the fetched messages
     */
    public static getMessageHistoryWith(
        opt: CwmHistoryOptionType = {}
    ): Promise<CwmMessage[]> {
        let link: string = `${[CWMChat.apiLink, 'webhooks', 'messages'].join(
            '/'
        )}${Object.keys(opt).length > 0 ? '?' : ''}`

        for (const [key, value] of Object.entries(opt)) {
            link += `${key}=${
                value instanceof Date ? value.toISOString() : value
            }&`
        }

        link = link.replace(/&$/, '')

        return new Promise<CwmMessage[]>((resolve, reject) => {
            axios
                .get(link, {
                    headers: {
                        Authorization: `Bearer ${this.instance.userToken}`,
                    },
                })
                .then((response: AxiosResponse) => {
                    let messages = response.data
                    let messageResponse = []
                    for (let msg of messages) {
                        messageResponse.push(
                            new CwmMessage(
                                msg.content,
                                msg.recipientId,
                                msg.externalUserId
                            )
                        )
                    }
                    resolve(messageResponse)
                })
                .catch(reject)
        })
    }

    /**
     * Rxjs Subject Hook to listen message channel
     *
     * @param callback
     */
    public static subscribeMessage(
        callback: (message: CwmMessage) => void
    ): void {
        this.instance._subject.subscribe(callback)
    }
}
