import {faker} from "@faker-js/faker"
import * as dotenv from 'dotenv'
import {CWMChat as cwmServer} from "@chatwithme/server"
import {CWMChat as cwmClient, CwmMessage} from "../src"

dotenv.config()

cwmServer.appToken = process.env.APP_TOKEN

const user = {
    id: faker.datatype.uuid(),
    name: faker.internet.userName()
}

test("Disconnect", async () => {
    const token = (await cwmServer.generateCWMUserToken({
        userName: user.name,
        userId: user.id
    })) as unknown as { token: string }

    await cwmClient.start(token.token)

    cwmClient.disconnect()
    expect(cwmClient.isStarted).toBeFalsy()
})


test("Start test", async () => {
    const token = (await cwmServer.generateCWMUserToken({
        userName: user.name,
        userId: user.id
    })) as unknown as { token: string }

    await cwmClient.start(token.token)

    expect(cwmClient.isStarted).toBeTruthy()
    cwmClient.disconnect()
})

test("Getter userId", async () => {
    const token = (await cwmServer.generateCWMUserToken({
        userName: user.name,
        userId: user.id
    })) as unknown as { token: string }

    await cwmClient.start(token.token)

    expect(cwmClient.userId).toBe(user.id)
    cwmClient.disconnect()
})

test("Send message when user not connected", async () => {
    await expect(
        cwmClient.sendMessage(new CwmMessage("Hello", faker.datatype.uuid(), user.id))
    ).rejects.toThrow()
})

test("Send message when user is connected", async () => {
    const token = (await cwmServer.generateCWMUserToken({
        userName: user.name,
        userId: user.id
    })) as unknown as { token: string }

    await cwmClient.start(token.token)

    await expect(
        cwmClient.sendMessage(new CwmMessage("Hello", faker.datatype.uuid(), user.id))
    ).resolves.toBe(undefined)

    cwmClient.disconnect()
})

test("Get message history without recipient", async () => {
    const token = (await cwmServer.generateCWMUserToken({
        userName: user.name,
        userId: user.id
    })) as unknown as { token: string }

    await cwmClient.start(token.token)

    await expect(cwmClient.getMessageHistoryWith({ })).rejects.toThrow()

    cwmClient.disconnect()
})

test("Get message history with recipient", async () => {
    const token = (await cwmServer.generateCWMUserToken({
        userName: user.name,
        userId: user.id
    })) as unknown as { token: string }

    await cwmClient.start(token.token)

    await expect(cwmClient.getMessageHistoryWith({ recipientId: faker.datatype.uuid() })).resolves.not.toBeNull()

    cwmClient.disconnect()
})
